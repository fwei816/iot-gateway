-- 用户表
create table if not exists IOT_USER
(
    ID          int primary key auto_increment comment '记录id',
    LOGIN_ID    varchar(50) not null comment '用户登录id',
    PASSWORD    varchar(50) not null comment '用户登录密码',
    NAME        varchar(50) not null comment '姓名',
    TELEPHONE   varchar(50) not null comment '电话号码',
    EMAIL       varchar(50) not null comment '邮件',
    STATUS      tinyint     not null default true comment '状态，已删除，未删除',
    CREATE_TIME timestamp   not null comment '创建时间',
    UPDATE_TIME timestamp   not null comment '更新时间',
    VERSION     int     not null default 0 comment '版本号'
) ENGINE = INNODB
DEFAULT CHARSET = utf8 COMMENT ='用户表';

-- 角色表
create table if not exists IOT_ROLE
(
    ID          int primary key auto_increment comment '记录id',
    CODE        varchar(50) not null comment '角色名称',
    CREATE_TIME timestamp   not null comment '创建时间',
    UPDATE_TIME timestamp   not null comment '更新时间'
) ENGINE = INNODB
DEFAULT CHARSET = utf8 COMMENT ='角色表';

-- 用户角色表
create table if not exists IOT_USER_ROLE
(
    USER_ID   int not null comment '用户id',
    ROLE_ID   int not null comment '角色id'
) ENGINE = INNODB
DEFAULT CHARSET = utf8 COMMENT ='用户角色表';

-- 角色权限表
create table if not exists IOT_ROLE_MENU
(
    ROLE_ID  int not null comment '角色id',
    MENU_ID  int not null comment '权限id'
) ENGINE = INNODB
DEFAULT CHARSET = utf8 COMMENT ='角色权限表';

-- 菜单表
create table if not exists IOT_MENU
(
    ID        int primary key comment '记录id',
    NAME      varchar(50) comment '菜单名称',
    ICON      varchar(50) comment '菜单图标',
    PATH      varchar(50) comment '路径',
    OPERATION varchar(2) comment '路径',
    PARENT_ID int        not null comment '父id'
) ENGINE = MYISAM
DEFAULT CHARSET = utf8 COMMENT ='菜单表';

insert into IOT_MENU(ID,NAME,ICON,PATH,OPERATION,PARENT_ID) values
('1', '首页', 'dashboard', 'dashboard', null, '0'),
('2', '系统管理', 'setting', '1', null, '0'),
('3', '用户管理', 'arrow-right', 'system-user', null, '2'),
('4', null, null, null, 'C', '3'),
('5', null, null, null, 'R', '3'),
('6', null, null, null, 'U', '3'),
('7', null, null, null, 'D', '3'),
('8', '权限管理', 'arrow-right', 'system-limit', null, '2'),
('9', null, null, null, 'C', '8'),
('10', null, null, null, 'R', '8'),
('11', null, null, null, 'U', '8'),
('12', null, null, null, 'D', '8');

insert into IOT_ROLE
(CODE, CREATE_TIME, UPDATE_TIME)
values
('管理员', current_timestamp, current_timestamp);

insert into IOT_ROLE
(CODE, CREATE_TIME, UPDATE_TIME)
values
('普通用户', current_timestamp, current_timestamp);

insert into IOT_ROLE_MENU values
('1', '1'),
('1', '2'),
('1', '3'),
('1', '4'),
('1', '5'),
('1', '6'),
('1', '7'),
('1', '8'),
('1', '9'),
('1', '10'),
('1', '11'),
('1', '12'),
('2', '1'),
('2', '2'),
('2', '3'),
('2', '5'),
('2', '8'),
('2', '10');

insert into IOT_USER
(LOGIN_ID,PASSWORD,NAME,TELEPHONE,EMAIL,STATUS,CREATE_TIME,UPDATE_TIME,VERSION)
values
('admin', 'MTIzNDU2', '管理员', '15988887777', '15988887777@163.com', true, current_timestamp, current_timestamp, 0);

insert into IOT_USER
(LOGIN_ID,PASSWORD,NAME,TELEPHONE,EMAIL,STATUS,CREATE_TIME,UPDATE_TIME,VERSION)
values
('guest', 'MTIzNDU2', '普通用户', '15988887979', '15988887979@163.com', true, current_timestamp, current_timestamp, 0);

insert into IOT_USER_ROLE values
('1', '1'),
('2', '2');