import React, { Component } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
//components
import Layout from "../components/layout/layout";
import Authorized from "../components/authorized/authorized";
//pages
import Login from "../pages/login/login";
import Dashboard from "../pages/dashboard/dashboard";
import DeviceManagement from "../pages/device/device-management";
import User from "../pages/system/user/user";
import Limit from "../pages/system/limit/limit";

class Router extends Component {
  render() {
    return (
      <HashRouter>
        <Switch>
          <Route path="/login" component={Login}></Route>
          <Route
            path="/"
            render={({ history, location, match }) => (
              <Layout history={history} location={location} match={match}>
                <Authorized location={location} match={match} />
                <Route
                  path="/dashboard"
                  pathname="dashboard"
                  component={Dashboard}
                />
                <Route
                  path="/device-management"
                  pathname="device-management"
                  component={DeviceManagement}
                />
                <Route
                  path="/system-user"
                  pathname="system-user"
                  component={User}
                />
                <Route
                  path="/system-limit"
                  pathname="system-limit"
                  component={Limit}
                />
              </Layout>
            )}
          ></Route>
        </Switch>
      </HashRouter>
    );
  }
}

export default Router;
