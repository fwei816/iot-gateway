import React, { Component } from "react";
import {
  Row,
  Col,
  Collapse,
  Form,
  Icon,
  PageHeader,
  Input,
  Button,
  Table,
  Tag,
  Tree,
  Modal,
  DatePicker,
  message,
} from "antd";
import moment from "moment";

import http from "../../../components/http/http";
import rest from "../../../assets/config/rest";

class Limit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "角色名称",
          dataIndex: "code",
          key: "code",
        },
        {
          title: "创建时间",
          dataIndex: "createTime",
          key: "createTime",
        },
        {
          title: "操作",
          key: "operation",
          width: 200,
          render: () => (
            <span>
              <Tag className="edit pointer" color="green">
                编辑
              </Tag>
              <Tag className="delete pointer" color="red">
                删除
              </Tag>
            </span>
          ),
        },
      ],
      searchForm: {
        code: "",
        startTime: null,
        endTime: null,
        value: null,
      },
      submitForm: {
        id: null,
        code: null,
        menuIds: [],
      },
      tableLoading: false,
      tableData: [],
      menuList: [],
      selectedMenus: [],
      showModal: false,
    };
    this.queryLimits = this.queryLimits.bind(this);
    this.handleResetTable = this.handleResetTable.bind(this);
    this.handleAddRole = this.handleAddRole.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTableRow = this.handleTableRow.bind(this);
    this.handleDatePicker = this.handleDatePicker.bind(this);
  }

  componentWillMount() {
    let that = this;
    http
      .get(rest.menuList)
      .then((response) => {
        that.setState({ menuList: response.data });
      })
      .catch((error) => {
        if (!!error.response && error.response.status === 401) {
          localStorage.removeItem(rest.token);
          localStorage.removeItem(rest.menu);
          that.props.history.push("/login");
        }
      });
    that.queryLimits();
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    let that = this;
    const { selectedMenus } = that.state;
    that.props.form.validateFields((err, values) => {
      if (!err) {
        let submitForm = values;
        submitForm.menuIds = selectedMenus.checked;
        if (!!submitForm.id) {
          http
            .post(rest.role, submitForm)
            .then((response) => {
              if (response.status === 200) {
                message.success("操作成功!");
                that.queryLimits();
              } else {
                message.error("操作失败");
              }
              that.setState({ showModal: false });
            })
            .catch((error) => {
              that.setState({ showModal: false });
              if (!!error.response && error.response.status === 401) {
                localStorage.removeItem(rest.token);
                localStorage.removeItem(rest.menu);
                that.props.history.push("/login");
              }
            });
        } else {
          http
            .put(rest.role, submitForm)
            .then((response) => {
              if (response.status === 200) {
                message.success("操作成功!");
                that.queryLimits();
              } else {
                message.error("操作失败");
              }
              that.setState({ showModal: false });
            })
            .catch((error) => {
              that.setState({ showModal: false });
              if (!!error.response && error.response.status === 401) {
                localStorage.removeItem(rest.token);
                localStorage.removeItem(rest.menu);
                that.props.history.push("/login");
              }
            });
        }
      }
    });
  }

  queryLimits() {
    let that = this;
    that.setState({ tableLoading: true });
    http
      .get(rest.roles, {
        params: that.state.searchForm,
      })
      .then((response) => {
        if (response.status === 200) {
          let result = response.data.map((x) => {
            x.createTime = moment(x.createTime).format("YYYY-MM-DD HH:mm:ss");
            return x;
          });
          that.setState({
            tableData: result,
            tableLoading: false,
          });
        }
      })
      .catch((error) => {
        that.setState({ tableLoading: false });
        if (!!error.response && error.response.status === 401) {
          localStorage.removeItem(rest.token);
          localStorage.removeItem(rest.menu);
          that.props.history.push("/login");
        }
      });
  }

  handleResetTable() {
    this.setState(
      {
        searchForm: {
          ...this.state.searchForm,
          code: "",
          startTime: null,
          endTime: null,
          value: null,
        },
      },
      () => {
        this.queryLimits();
      }
    );
  }

  handleAddRole() {
    this.props.form.resetFields();
    this.setState({ showModal: true });
  }

  handleTableRow(record) {
    let that = this;
    const { setFieldsValue, resetFields } = that.props.form;
    return {
      onClick(event) {
        let node = event.target.className;
        if (node.indexOf("edit") !== -1) {
          resetFields();
          setFieldsValue({
            id: record.id,
            code: record.code,
          });
          that.setState({ selectedMenus: record.menuIds, showModal: true });
        } else if (node.indexOf("delete") !== -1) {
          Modal.confirm({
            title: "Confirm",
            content: "确认删除此角色吗？",
            okText: "确认",
            cancelText: "取消",
            onOk: () => {
              http
                .delete(`${rest.role}/${record.id}`)
                .then((response) => {
                  if (response.status === 200) {
                    message.success("操作成功！");
                  }
                  that.queryLimits();
                })
                .catch((error) => {
                  message.error("操作失败！");
                  if (!!error.response && error.response.status === 401) {
                    localStorage.removeItem(rest.token);
                    localStorage.removeItem(rest.menu);
                    that.props.history.push("/login");
                  }
                });
            },
          });
        }
      },
    };
  }

  handleRenderTreeNodes(data) {
    return data.map((item) => {
      if (item.children) {
        return (
          <Tree.TreeNode title={item.name} key={item.id} dataRef={item}>
            {this.handleRenderTreeNodes(item.children)}
          </Tree.TreeNode>
        );
      }
      return <Tree.TreeNode key={item.id} title={item.name} />;
    });
  }

  handleDatePicker(date, dateString) {
    let temp = date.map((x) => {
      return x.valueOf();
    });
    this.setState({
      searchForm: {
        ...this.state.searchForm,
        startTime: temp[0],
        endTime: temp[1],
        value: date,
      },
    });
  }

  render() {
    const {
      columns,
      tableData,
      tableLoading,
      searchForm,
      submitForm,
      menuList,
      selectedMenus,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };
    return (
      <Row>
        <Col>
          <PageHeader
            style={{ paddingLeft: 0, paddingTop: 0 }}
            title={
              <span>
                <Icon type="tag" />
                &nbsp;权限管理
              </span>
            }
          />
          <Row>
            <Col>
              <Collapse defaultActiveKey={["1"]} expandIconPosition="right">
                <Collapse.Panel
                  header={
                    <span>
                      <Icon type="search" />
                      &nbsp;查询条件
                    </span>
                  }
                  key="1"
                >
                  <Row>
                    <Col span={3}>
                      <label>权限名称:</label>
                      <Input
                        placeholder="请输入权限名称"
                        type="text"
                        value={searchForm.code}
                        onChange={(e) => {
                          this.setState({
                            searchForm: {
                              ...this.state.searchForm,
                              code: e.target.value,
                            },
                          });
                        }}
                      />
                    </Col>
                    <Col style={{ marginLeft: "0.5rem" }} span={5}>
                      <label>创建时间:</label>
                      <DatePicker.RangePicker
                        value={searchForm.value}
                        onChange={this.handleDatePicker}
                      />
                    </Col>
                    <Col style={{ marginLeft: "0.5rem" }} span={10}>
                      <label style={{ display: "block" }}>&nbsp;</label>
                      <div>
                        <Button onClick={this.queryLimits} type="primary">
                          <Icon type="search" /> 查询
                        </Button>
                        <Button
                          onClick={this.handleResetTable}
                          style={{ marginLeft: "0.5rem" }}
                          type="primary"
                        >
                          <Icon type="reload" /> 重置
                        </Button>
                        <Button
                          onClick={this.handleAddRole}
                          style={{ marginLeft: "0.5rem" }}
                          type="primary"
                        >
                          <Icon type="plus" /> 新增角色
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </Collapse.Panel>
              </Collapse>
            </Col>
          </Row>
          <Row style={{ marginTop: "1rem" }}>
            <Col>
              <Table
                rowKey="id"
                loading={tableLoading}
                onRow={this.handleTableRow}
                pagination={false}
                bordered
                dataSource={tableData}
                columns={columns}
              />
            </Col>
          </Row>
          <Modal
            title="权限编辑/修改"
            visible={this.state.showModal}
            onOk={this.handleSubmit}
            onCancel={() => this.setState({ showModal: false })}
          >
            <Form {...formItemLayout}>
              <Form.Item style={{ display: "none" }}>
                {getFieldDecorator("id")(<Input />)}
              </Form.Item>
              <Form.Item label="权限名称:">
                {getFieldDecorator("code", {
                  initialValue: submitForm.code,
                  rules: [
                    {
                      required: true,
                      message: "Please input your code!",
                    },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item label="配置菜单:">
                <Tree
                  checkable
                  checkStrictly
                  onCheck={(checkedKeys) =>
                    this.setState({ selectedMenus: checkedKeys })
                  }
                  checkedKeys={selectedMenus}
                >
                  {this.handleRenderTreeNodes(menuList)}
                </Tree>
              </Form.Item>
            </Form>
          </Modal>
        </Col>
      </Row>
    );
  }
}

export default Form.create()((props) => (
  <Limit {...props} key={props.location.pathname} />
));
