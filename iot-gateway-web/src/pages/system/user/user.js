import React, { Component } from "react";
import {
  Row,
  Col,
  Collapse,
  Form,
  Icon,
  PageHeader,
  Input,
  Button,
  Table,
  Tag,
  Select,
  Modal,
  message,
} from "antd";

import http from "../../../components/http/http";
import rest from "../../../assets/config/rest";

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "登录号",
          dataIndex: "loginId",
          key: "loginId",
        },
        {
          title: "姓名",
          dataIndex: "name",
          key: "name",
        },
        {
          title: "电话",
          dataIndex: "telephone",
          key: "telephone",
        },
        {
          title: "邮箱",
          dataIndex: "email",
          key: "email",
        },
        {
          title: "角色",
          dataIndex: "roleVo.code",
          key: "roleVo.code",
        },
        {
          title: "操作",
          key: "operation",
          width: 200,
          render: () => (
            <span>
              <Tag className="edit pointer" color="green">
                编辑
              </Tag>
              <Tag className="delete pointer" color="red">
                删除
              </Tag>
            </span>
          ),
        },
      ],
      roles: [],
      searchForm: {
        name: "",
        pageNum: 0,
        pageSize: 10,
      },
      disableItem: false,
      submitForm: {
        id: "",
        loginId: "",
        password: "",
        name: "",
        telephone: "",
        email: "",
        roleId: "",
      },
      tableLoading: false,
      tableData: {
        total: 0,
        result: [],
      },
      showModal: false,
    };
    this.queryUsers = this.queryUsers.bind(this);
    this.handleChangePageSize = this.handleChangePageSize.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleResetTable = this.handleResetTable.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTableRow = this.handleTableRow.bind(this);
  }

  componentDidMount() {
    let that = this;
    http
      .get(rest.roles)
      .then((response) => {
        that.setState({ roles: response.data });
      })
      .catch((error) => {
        if (!!error.response && error.response.status === 401) {
          localStorage.removeItem(rest.token);
          localStorage.removeItem(rest.menu);
          that.props.history.push("/login");
        }
      });
    that.queryUsers();
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    let that = this;
    that.props.form.validateFields((err, values) => {
      if (!err) {
        if (!!values.id) {
          let submitForm = values;
          submitForm.loginId = null;
          submitForm.password = null;
          http
            .post(rest.user, submitForm)
            .then((response) => {
              if (response.status === 200) {
                message.success("操作成功!");
                that.queryUsers();
              } else {
                message.error("操作失败");
              }
              that.setState({ showModal: false });
            })
            .catch((error) => {
              that.setState({ showModal: false });
              if (!!error.response && error.response.status === 401) {
                localStorage.removeItem(rest.token);
                localStorage.removeItem(rest.menu);
                that.props.history.push("/login");
              }
            });
        } else {
          let submitForm = values;
          submitForm.password = window.btoa(submitForm.password);
          http
            .put(rest.user, submitForm)
            .then((response) => {
              if (response.status === 200) {
                message.success("操作成功!");
                that.queryUsers();
              } else {
                message.error("操作失败");
              }
              that.setState({ showModal: false });
            })
            .catch((error) => {
              that.setState({ showModal: false });
              if (!!error.response && error.response.status === 401) {
                localStorage.removeItem(rest.token);
                localStorage.removeItem(rest.menu);
                that.props.history.push("/login");
              }
            });
        }
      }
    });
  }

  queryUsers() {
    let that = this;
    that.setState({ tableLoading: true });
    http
      .get(rest.users, {
        params: that.state.searchForm,
      })
      .then((response) => {
        if (response.status === 200) {
          let data = response.data.result;
          let result =
            data.length === 0
              ? []
              : data.map((obj) => {
                  obj.key = obj.id;
                  return obj;
                });
          that.setState({
            tableData: {
              total: response.data.total,
              result: result,
            },
            tableLoading: false,
          });
        }
      })
      .catch((error) => {
        that.setState({ tableLoading: false });
        if (!!error.response && error.response.status === 401) {
          localStorage.removeItem(rest.token);
          localStorage.removeItem(rest.menu);
          that.props.history.push("/login");
        }
      });
  }

  handleChangePageSize(current, pageSize) {
    this.setState(
      {
        searchForm: {
          ...this.state.searchForm,
          pageNum: current,
          pageSize: pageSize,
        },
      },
      () => {
        this.queryUsers();
      }
    );
  }

  handleChangePage(current) {
    this.setState(
      {
        searchForm: {
          ...this.state.searchForm,
          pageNum: current,
        },
      },
      () => {
        this.queryUsers();
      }
    );
  }

  handleResetTable() {
    this.setState(
      {
        searchForm: {
          ...this.state.searchForm,
          name: "",
        },
      },
      () => {
        this.queryUsers();
      }
    );
  }

  handleAddUser() {
    this.props.form.resetFields();
    this.setState({ showModal: true, disableItem: false });
  }

  handleTableRow(record) {
    let that = this;
    const { setFieldsValue, resetFields } = that.props.form;
    return {
      onClick(event) {
        let node = event.target.className;
        if (node.indexOf("edit") !== -1) {
          resetFields();
          that.setState({ showModal: true, disableItem: true }, () => {
            let obj = {
              id: record.id,
              loginId: record.loginId,
              password: record.password,
              name: record.name,
              telephone: record.telephone,
              email: record.email,
              roleId: record.roleVo.id,
            };
            setFieldsValue(obj);
          });
        } else if (node.indexOf("delete") !== -1) {
          Modal.confirm({
            title: "Confirm",
            content: "确认删除此用户吗",
            okText: "确认",
            cancelText: "取消",
            onOk: () => {
              http
                .delete(`${rest.user}/${record.id}`)
                .then((response) => {
                  if (response.status === 200) {
                    message.success("操作成功！");
                  }
                  that.queryUsers();
                })
                .catch((error) => {
                  message.error("操作失败！");
                  if (!!error.response && error.response.status === 401) {
                    localStorage.removeItem(rest.token);
                    localStorage.removeItem(rest.menu);
                    that.props.history.push("/login");
                  }
                });
            },
          });
        }
      },
    };
  }

  render() {
    const {
      columns,
      tableData,
      tableLoading,
      searchForm,
      submitForm,
      roles,
      disableItem,
    } = this.state;
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    };
    return (
      <Row>
        <Col>
          <PageHeader
            style={{ paddingLeft: 0, paddingTop: 0 }}
            title={
              <span>
                <Icon type="user" />
                &nbsp;用户管理
              </span>
            }
          />
          <Row>
            <Col>
              <Collapse defaultActiveKey={["1"]} expandIconPosition="right">
                <Collapse.Panel
                  header={
                    <span>
                      <Icon type="search" />
                      &nbsp;查询条件
                    </span>
                  }
                  key="1"
                >
                  <Row>
                    <Col span={3}>
                      <label>姓名:</label>
                      <Input
                        placeholder="请输入姓名"
                        type="text"
                        value={searchForm.name}
                        onChange={(e) => {
                          this.setState({
                            searchForm: {
                              ...this.state.searchForm,
                              name: e.target.value,
                            },
                          });
                        }}
                      />
                    </Col>
                    <Col style={{ marginLeft: "0.5rem" }} span={20}>
                      <label style={{ display: "block" }}>&nbsp;</label>
                      <div>
                        <Button onClick={this.queryUsers} type="primary">
                          <Icon type="search" /> 查询
                        </Button>
                        <Button
                          onClick={this.handleResetTable}
                          style={{ marginLeft: "0.5rem" }}
                          type="primary"
                        >
                          <Icon type="reload" /> 重置
                        </Button>
                        <Button
                          onClick={this.handleAddUser}
                          style={{ marginLeft: "0.5rem" }}
                          type="primary"
                        >
                          <Icon type="plus" /> 新增用户
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </Collapse.Panel>
              </Collapse>
            </Col>
          </Row>
          <Row style={{ marginTop: "1rem" }}>
            <Col>
              <Table
                loading={tableLoading}
                rowKey="id"
                onRow={this.handleTableRow}
                pagination={{
                  showSizeChanger: true,
                  showQuickJumper: false,
                  pageSize: searchForm.pageSize,
                  current: searchForm.pageNum,
                  total: tableData.total,
                  showTotal: () => `共${tableData.total}条`,
                  onShowSizeChange: this.handleChangePageSize,
                  onChange: this.handleChangePage,
                }}
                bordered
                dataSource={tableData.result}
                columns={columns}
              />
            </Col>
          </Row>
          <Modal
            title="用户编辑/修改"
            visible={this.state.showModal}
            onOk={this.handleSubmit}
            onCancel={() => this.setState({ showModal: false })}
          >
            <Form {...formItemLayout}>
              <Form.Item style={{ display: "none" }}>
                {getFieldDecorator("id")(<Input />)}
              </Form.Item>
              <Form.Item label="登录名:">
                {getFieldDecorator("loginId", {
                  initialValue: submitForm.loginId,
                  rules: [
                    {
                      required: true,
                      message: "Please input your loginId!",
                    },
                  ],
                })(<Input disabled={disableItem} />)}
              </Form.Item>
              <Form.Item label="密码:">
                {getFieldDecorator("password", {
                  initialValue: submitForm.password,
                  rules: [
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                  ],
                })(<Input.Password disabled={disableItem} />)}
              </Form.Item>
              <Form.Item label="姓名:">
                {getFieldDecorator("name", {
                  initialValue: submitForm.name,
                  rules: [
                    {
                      required: true,
                      message: "Please input your name!",
                    },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item label="角色:">
                {getFieldDecorator("roleId", {
                  initialValue: submitForm.roleId,
                  rules: [
                    {
                      required: true,
                      message: "Please select your role!",
                    },
                  ],
                })(
                  <Select>
                    {roles.map((role) => {
                      return (
                        <Select.Option key={role.id} value={role.id}>
                          {role.code}
                        </Select.Option>
                      );
                    })}
                  </Select>
                )}
              </Form.Item>
              <Form.Item label="电话号码:">
                {getFieldDecorator("telephone", {
                  initialValue: submitForm.telephone,
                  rules: [
                    {
                      required: true,
                      message: "Please input your telephone!",
                    },
                  ],
                })(<Input />)}
              </Form.Item>
              <Form.Item label="邮箱:">
                {getFieldDecorator("email", {
                  initialValue: submitForm.email,
                  rules: [
                    {
                      required: true,
                      message: "Please input your email!",
                    },
                  ],
                })(<Input />)}
              </Form.Item>
            </Form>
          </Modal>
        </Col>
      </Row>
    );
  }
}

export default Form.create()((props) => (
  <User {...props} key={props.location.pathname} />
));
