import React, { Component } from "react";
import { Form, Icon, Input, Button } from "antd";

import http from "../../components/http/http";
import rest from "../../assets/config/rest";

import LoginCss from "./login.module.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillUnmount() {
    this.setState = (state, callback) => {
      return;
    };
  }

  handleSubmit(e) {
    let that = this;
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ loading: true });
        let submitForm = values;
        submitForm.password = window.btoa(values.password);
        http
          .post(rest.login, submitForm)
          .then(response => {
            if (response.status === 200) {
              localStorage.setItem(
                rest.token,
                JSON.stringify({
                  token: response.data.token,
                  period: response.data.tokenPeriod
                })
              );
              localStorage.setItem(
                rest.menu,
                JSON.stringify(response.data.menus)
              );
              that.props.history.push("/dashboard");
            }
            that.setState({ loading: false });
          })
          .catch(error => {
            that.setState({ loading: false });
          });
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className={LoginCss.loginForm}>
        <h1 className={LoginCss.title}>Please input your information</h1>
        <Form.Item>
          {getFieldDecorator("loginId", {
            rules: [{ required: true, message: "Please input your username!" }]
          })(
            <Input
              suffix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
              placeholder="Username"
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Input
              suffix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
              type="password"
              placeholder="Password"
            />
          )}
        </Form.Item>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            size="large"
            loading={this.state.loading}
            className={LoginCss.loginFormButton}
          >
            Log in
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Form.create()(props => (
  <Login {...props} key={props.location.pathname} />
));
