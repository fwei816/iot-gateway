import React, { Component } from "react";
import { Row, Col, Icon, Card, PageHeader } from "antd";
import moment from "moment";

import http from "../../components/http/http";
import rest from "../../assets/config/rest";

class DeviceManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {} = this.state;
    return (
      <Row>
        <Col>
          <PageHeader
            style={{ paddingLeft: 0, paddingTop: 0 }}
            title={
              <span>
                <Icon type="bell" />
                &nbsp;设备管理
              </span>
            }
          />
          <Row gutter={8}>
            <Col span={6}>
              <Card
                title="Card title"
                extra={<a href="#">More</a>}
              >
                <p>Card content</p>
                <p>Card content</p>
                <p>Card content</p>
              </Card>
            </Col>
            <Col span={18}>
              <Card
                title="Card title"
                extra={<a href="#">More</a>}
              >
                <p>Card content</p>
                <p>Card content</p>
                <p>Card content</p>
              </Card>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default (props) => (
  <DeviceManagement {...props} key={props.location.pathname} />
);
