import React, { Component } from "react";
import {
  Row,
  Col,
  Layout as AntLayout,
  Icon,
  Menu,
  Avatar,
  Dropdown
} from "antd";

import http from "../../components/http/http";
import rest from "../../assets/config/rest";

import LayoutCss from "./layout.module.css";

const { Header, Sider, Content } = AntLayout;

class Layout extends Component {
  constructor(props) {
    super(props);
    let path = this.props.location.pathname.split("/")[1];
    this.state = {
      collapsed: false,
      activePath: !!path ? path : "dashboard",
      menus: []
    };
    this.toggleMenu = this.toggleMenu.bind(this);
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentDidMount() {
    let menus = window.localStorage.getItem(rest.menu);
    if (!!menus) {
      this.setState({ menus: JSON.parse(menus) });
    }
  }

  toggleMenu() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  handleLogout() {
    http.get(rest.logout).then(response => {
      if (response.status === 200) {
        localStorage.removeItem(rest.token);
        localStorage.removeItem(rest.menu);
        this.props.history.push("/login");
      }
    });
  }

  handleMenuClick(item) {
    this.props.history.push(item.key);
  }

  render() {
    const { collapsed, activePath, menus } = this.state;
    return (
      <AntLayout className={LayoutCss.layoutContent}>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className={LayoutCss.logo} />
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={[activePath]}
            onClick={this.handleMenuClick}
          >
            {menus.map(menu => {
              if (!!menu.children) {
                return (
                  <Menu.SubMenu
                    key={menu.id}
                    title={
                      <span>
                        <Icon type={menu.icon} />
                        <span>{menu.name}</span>
                      </span>
                    }
                  >
                    {menu.children.map(child => {
                      return (
                        <Menu.Item key={child.path}>
                          <Icon type={child.icon} />
                          <span>{child.name}</span>
                        </Menu.Item>
                      );
                    })}
                  </Menu.SubMenu>
                );
              } else {
                return (
                  <Menu.Item key={menu.path}>
                    <Icon type={menu.icon} />
                    <span>{menu.name}</span>
                  </Menu.Item>
                );
              }
            })}
          </Menu>
        </Sider>
        <AntLayout className={LayoutCss.siteLayout}>
          <Header
            className={LayoutCss.siteLayoutBackground}
            style={{ padding: 0 }}
          >
            <Row>
              <Col span={23}>
                <Icon
                  className={LayoutCss.trigger}
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggleMenu}
                />
              </Col>
              <Col span={1}>
                <Dropdown
                  overlay={
                    <Menu>
                      <Menu.Item>
                        <span>修改密码</span>
                      </Menu.Item>
                      <Menu.Item onClick={this.handleLogout}>
                        <span>退出</span>
                      </Menu.Item>
                    </Menu>
                  }
                >
                  <Avatar size="large" icon="user" />
                </Dropdown>
              </Col>
            </Row>
          </Header>
          <Content
            className={`${LayoutCss.siteLayoutBackground} ${LayoutCss.siteLayoutContent}`}
          >
            {this.props.children}
          </Content>
        </AntLayout>
      </AntLayout>
    );
  }
}

export default Layout;
