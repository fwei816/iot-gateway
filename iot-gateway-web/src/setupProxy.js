const { createProxyMiddleware } =  require("http-proxy-middleware");

module.exports = function(app) {
  app.use(createProxyMiddleware("/api/v1.0/", { target: "http://127.0.0.1:8080/" }));
};
