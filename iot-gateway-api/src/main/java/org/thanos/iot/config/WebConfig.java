package org.thanos.iot.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thanos.iot.interceptor.AuthInterceptor;
import org.thanos.iot.service.session.SessionService;
import org.thanos.iot.service.session.SessionServiceImpl;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${session.timeout}")
    private int timeout;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor())
                .addPathPatterns("/api/v1.0/**")
                .excludePathPatterns("/api/v1.0/login");
    }

    @Bean(name = "sessionService")
    public SessionService sessionService() {
        return new SessionServiceImpl(timeout);
    }

    @Bean(name = "authInterceptor")
    public AuthInterceptor authInterceptor() {
        return new AuthInterceptor();
    }
}
