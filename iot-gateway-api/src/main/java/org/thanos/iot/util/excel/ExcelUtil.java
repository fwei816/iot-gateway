package org.thanos.iot.util.excel;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class ExcelUtil {

    private static final String EXCEL_2007 = ".xlsx";
    private static final String EXCEL_2003 = ".xls";

    /**
     * 解析excel
     *
     * @param file
     * @param function 转换参数处理
     * @param <R>
     * @return
     * @throws Exception
     */
    public static <R> List<R> readExcel(MultipartFile file, Integer startRow, Function<Row, R> function) throws Exception {
        Assert.isTrue(Optional.ofNullable(file).isPresent(), "file must not be null");
        List<R> results = new ArrayList<>();
        String fileName = file.getOriginalFilename();
        Assert.isTrue(!StringUtils.isEmpty(fileName), "fileName must not be null");
        Workbook workbook;
        if (fileName.endsWith(EXCEL_2007)) {
            workbook = new XSSFWorkbook(file.getInputStream());
        } else if (fileName.endsWith(EXCEL_2003)) {
            workbook = new HSSFWorkbook(file.getInputStream());
        } else {
            throw new Exception("Unsupported excel type");
        }
        int sheetNum = workbook.getNumberOfSheets();
        for (int i = 0; i < sheetNum; i++) {
            Sheet sheet1 = workbook.getSheetAt(i);
            //获取当前sheet页的总行数
            int totalRowNumber = sheet1.getPhysicalNumberOfRows();
            for (int j = startRow; j < totalRowNumber; j++) {
                Row row = sheet1.getRow(j);
                results.add(function.apply(row));
            }
        }
        return results;
    }

    public static String getCellValue(Cell cell) {
        if (CellType.BOOLEAN.equals(cell.getCellType())) {
            return String.valueOf(cell.getBooleanCellValue());
        } else if (CellType.NUMERIC.equals(cell.getCellType())) {
            double d = cell.getNumericCellValue();
            return String.valueOf(d);
        }
        return String.valueOf(cell.getStringCellValue());
    }
}
