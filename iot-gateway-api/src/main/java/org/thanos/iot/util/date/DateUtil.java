package org.thanos.iot.util.date;

import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    private static final String DEFAULT_DATETIME_FORMAT = "YYYY-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_FORMAT = "YYYY-MM-dd";
    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    public static String currentDateTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DEFAULT_DATETIME_FORMAT));
    }

    public static String currentTime() {
        return LocalTime.now().format(DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT));
    }

    public static String currentDate() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT));
    }

    public static String timestampToDateTime(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DEFAULT_DATETIME_FORMAT));
    }

    /**
     * 从此刻增加多少分钟后的时间戳
     * @param minutes
     * @return
     */
    public static long nowPlusMinuteToTimestamp(long minutes) {
        return Timestamp.valueOf(LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault()).plusMinutes(minutes)).getTime();
    }
}
