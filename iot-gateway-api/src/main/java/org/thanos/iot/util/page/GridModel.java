package org.thanos.iot.util.page;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class GridModel<T> {

    @JsonProperty("total")
    private long total;

    @JsonProperty("result")
    private List<T> result;

    public static <T> GridModel<T> of(long total, List<T> result) {
        return new GridModel<>(total, result);
    }

    private GridModel(long total, List<T> result) {
        this.total = total;
        this.result = result;
    }
}
