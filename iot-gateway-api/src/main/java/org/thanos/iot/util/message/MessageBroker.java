package org.thanos.iot.util.message;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class MessageBroker {

    private static final Map<String, BlockingQueue<String>> MESSAGE = new ConcurrentHashMap<>();

    public static void publish(String topic, String message) {
        BlockingQueue<String> queue = MESSAGE.get(topic);
        if (Objects.isNull(queue)) {
            queue = new LinkedBlockingQueue<>();
        }
        queue.add(message);
        MESSAGE.put(topic, queue);
    }

    public static void subscribe(String topic, Consumer<String> consumer) throws InterruptedException {
        BlockingQueue<String> queue = MESSAGE.get(topic);
        if (Objects.isNull(queue)) {
            return;
        }
        String message = queue.take();
        MESSAGE.put(topic, queue);
        consumer.accept(message);
    }

    public static void main(String[] args) throws InterruptedException {
        CompletableFuture.runAsync(() -> {
            for (; ; ) {
                try {
                    Thread.sleep(0);
                    MessageBroker.subscribe("abc", System.out::println);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        while (true) {
            TimeUnit.SECONDS.sleep(Math.round(Math.random()*10));
            MessageBroker.publish("abc", UUID.randomUUID().toString());
        }

    }
}
