package org.thanos.iot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication(scanBasePackages = "org.thanos.iot")
@MapperScan(basePackages = "org.thanos.iot.mapper")
public class ProjectBootstrap {

    public static void main(String[] args) {
        new SpringApplicationBuilder(ProjectBootstrap.class)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }


}
