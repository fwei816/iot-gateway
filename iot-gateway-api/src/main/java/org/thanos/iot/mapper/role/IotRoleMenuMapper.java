package org.thanos.iot.mapper.role;

import org.thanos.iot.entity.role.IotRoleMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IotRoleMenuMapper {

    int insertForeach(@Param("list") List<IotRoleMenu> record);

    List<Integer> queryByRoleId(@Param("roleId") Integer roleId);

    int deleteByRoleId(@Param("roleId") Integer roleId);
}