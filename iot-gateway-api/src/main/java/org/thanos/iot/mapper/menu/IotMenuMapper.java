package org.thanos.iot.mapper.menu;

import org.thanos.iot.entity.menu.IotMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IotMenuMapper {

    List<IotMenu> queryAll();

    List<IotMenu> queryByMenuIds(@Param("list") List<Integer> menuIds);
}