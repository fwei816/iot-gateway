package org.thanos.iot.entity.menu;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class IotMenu {

    private Integer id;
    private String name;
    private String path;
    private String icon;
    private String operation;
    private Integer parentId;
}