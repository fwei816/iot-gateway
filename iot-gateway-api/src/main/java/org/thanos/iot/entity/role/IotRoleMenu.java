package org.thanos.iot.entity.role;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class IotRoleMenu {

    private Integer roleId;
    private Integer menuId;
}