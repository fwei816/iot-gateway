package org.thanos.iot.entity.role;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Timestamp;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class IotRole {

    private Integer id;
    private String code;
    private Timestamp createTime;
    private Timestamp updateTime;
}