package org.thanos.iot.entity.session;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class SessionEntity {

    private int id;
    private String loginId;
    private long timestamp;
    private String uuid;
}
