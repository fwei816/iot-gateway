package org.thanos.iot.entity.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Timestamp;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class IotUser {

    private Integer id;
    private String loginId;
    private String password;
    private String name;
    private String telephone;
    private String email;
    private Boolean status;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Integer version;
}