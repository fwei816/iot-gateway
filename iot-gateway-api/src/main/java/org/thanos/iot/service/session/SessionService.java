package org.thanos.iot.service.session;

import org.springframework.validation.annotation.Validated;
import org.thanos.iot.entity.session.SessionEntity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
public interface SessionService {

    long saveSession(@Valid @NotNull String key, @Valid @NotNull SessionEntity sessionEntity);

    SessionEntity getSession(@Valid @NotNull String key);

    boolean checkSession(@Valid @NotNull String key);

    void removeSession(@Valid @NotNull String key);
}
