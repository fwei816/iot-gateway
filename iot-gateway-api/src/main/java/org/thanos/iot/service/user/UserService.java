package org.thanos.iot.service.user;

import org.springframework.validation.annotation.Validated;
import org.thanos.iot.controller.user.UserVo;
import org.thanos.iot.util.page.GridModel;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Validated
public interface UserService {

    void addUser(@Valid @NotBlank String loginId, @Valid @NotBlank String password, @Valid @NotBlank String name, @Valid @NotBlank String telephone, @Valid @NotBlank String email, Integer roleId);

    void modifyUser(@Valid @NotNull Integer id, String name, String telephone, String email, Integer roleId);

    void modifyUserPassword(@Valid @NotNull Integer userId, @Valid @NotBlank String password);

    UserVo discoverUser(@Valid @NotNull Integer userId);

    boolean discoverUserByLoginId(@Valid @NotBlank String loginId, Integer id);

    GridModel<UserVo> discoverUsers(String name, @Valid @NotNull Integer pageNum, @Valid @NotNull Integer pageSize);

    void deleteUser(@Valid @NotNull Integer userId);
}
