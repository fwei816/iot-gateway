package org.thanos.iot.service.menu;

import org.thanos.iot.controller.menu.MenuVo;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public interface MenuService {

    /**
     * 获取全部菜单
     * @return
     */
    List<MenuVo> queryAll();

    /**
     * 根据id获取菜单
     * @param menuIds
     * @return
     */
    List<MenuVo> queryByMenuIds(@Valid @NotNull List<Integer> menuIds);

    /**
     * 为前端角色配置权限
     * @return
     */
    List<MenuVo> limitMenuList();
}
