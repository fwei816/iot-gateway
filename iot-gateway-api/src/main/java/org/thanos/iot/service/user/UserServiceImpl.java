package org.thanos.iot.service.user;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.thanos.iot.controller.user.UserVo;
import org.thanos.iot.entity.user.IotUser;
import org.thanos.iot.entity.user.IotUserRole;
import org.thanos.iot.mapper.user.IotUserMapper;
import org.thanos.iot.mapper.user.IotUserRoleMapper;
import org.thanos.iot.service.role.RoleService;
import org.thanos.iot.util.page.GridModel;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private IotUserMapper userMapper;

    @Autowired
    private IotUserRoleMapper userRoleMapper;

    @Autowired
    private RoleService roleService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void addUser(String loginId, String password, String name, String telephone, String email, Integer roleId) {
        Assert.isTrue(!Optional.ofNullable(userMapper.selectByLoginId(loginId, null)).isPresent(), "loginId is exists");
        IotUser user = new IotUser();
        user.setLoginId(loginId);
        user.setPassword(password);
        user.setName(name);
        user.setTelephone(telephone);
        user.setEmail(email);
        user.setStatus(Boolean.TRUE);
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        user.setCreateTime(timestamp);
        user.setUpdateTime(timestamp);
        user.setVersion(0);
        userMapper.insertSelective(user);
        Optional.ofNullable(roleId).ifPresent(role -> {
            IotUserRole userRole = new IotUserRole();
            userRole.setUserId(user.getId());
            userRole.setRoleId(role);
            userRoleMapper.insertSelective(userRole);
        });

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void modifyUser(Integer id, String name, String telephone, String email, Integer roleId) {
        IotUser user = userMapper.selectByPrimaryKey(id);
        Assert.isTrue(Optional.ofNullable(user).isPresent(), "user is not exists");
        user.setName(name);
        user.setTelephone(telephone);
        user.setEmail(email);
        user.setUpdateTime(Timestamp.valueOf(LocalDateTime.now()));
        userMapper.updateByPrimaryKeySelective(user);
        Optional.ofNullable(roleId).ifPresent(role -> userRoleMapper.updateByUserId(id, role));
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void modifyUserPassword(Integer userId, String password) {
        IotUser user = userMapper.selectByPrimaryKey(userId);
        Assert.isTrue(Optional.ofNullable(user).isPresent(), "user is not exists");
        user.setPassword(password);
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public UserVo discoverUser(Integer userId) {
        IotUser user = userMapper.selectByPrimaryKey(userId);
        if (Objects.isNull(user)) {
            return null;
        }
        UserVo userVo = new UserVo();
        userVo.setId(user.getId());
        userVo.setLoginId(user.getLoginId());
        userVo.setPassword(user.getPassword());
        userVo.setName(user.getName());
        userVo.setTelephone(user.getTelephone());
        userVo.setEmail(user.getEmail());
        Optional.ofNullable(userRoleMapper.selectByUserId(userId)).ifPresent(userRole -> userVo.setRoleVo(roleService.discoverOne(userRole.getRoleId())));
        return userVo;
    }

    @Override
    public boolean discoverUserByLoginId(String loginId, Integer id) {
        IotUser commonUser = userMapper.selectByLoginId(loginId, id);
        return Optional.ofNullable(commonUser).isPresent();
    }

    @Override
    public GridModel<UserVo> discoverUsers(String name, Integer pageNum, Integer pageSize) {
        Page<IotUser> page = PageHelper.startPage(pageNum, pageSize, true);
        userMapper.queryByParams(name);
        PageInfo<IotUser> info = PageInfo.of(page.getResult());
        List<UserVo> results = page.getResult().stream().map(user -> {
            UserVo userVo = new UserVo();
            userVo.setId(user.getId());
            userVo.setLoginId(user.getLoginId());
            userVo.setPassword(user.getPassword());
            userVo.setName(user.getName());
            userVo.setTelephone(user.getTelephone());
            userVo.setEmail(user.getEmail());
            Optional.ofNullable(userRoleMapper.selectByUserId(user.getId())).ifPresent(userRole -> userVo.setRoleVo(roleService.discoverOne(userRole.getRoleId())));
            return userVo;
        }).collect(Collectors.toList());
        return GridModel.of(info.getTotal(), results);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteUser(Integer userId) {
        IotUser user = userMapper.selectByPrimaryKey(userId);
        Assert.isTrue(Optional.ofNullable(user).isPresent(), "can not find user");
        user.setStatus(Boolean.FALSE);
        userMapper.updateByPrimaryKeySelective(user);
        userRoleMapper.deleteByPrimaryKey(userId);
    }
}
