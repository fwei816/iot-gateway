package org.thanos.iot.service.session;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.thanos.iot.entity.session.SessionEntity;
import org.thanos.iot.util.date.DateUtil;

import java.time.Duration;
import java.util.Optional;

public class SessionServiceImpl implements SessionService {

    private final Cache<String, SessionEntity> cache;
    private final int minutes;

    public SessionServiceImpl(int minutes) {
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build(true);
        CacheConfiguration<String, SessionEntity> cacheConfiguration = CacheConfigurationBuilder
                .newCacheConfigurationBuilder(String.class, SessionEntity.class, ResourcePoolsBuilder.heap(100))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofMinutes(minutes))).build();
        // 根据配置创建一个缓存容器
        this.cache = cacheManager.createCache("session", cacheConfiguration);
        this.minutes = minutes;
    }

    @Override
    public long saveSession(String key, SessionEntity entity) {
        entity.setTimestamp(System.currentTimeMillis());
        cache.put(key, entity);
        return DateUtil.nowPlusMinuteToTimestamp(this.minutes);
    }

    @Override
    public SessionEntity getSession(String key) {
        return cache.get(key);
    }

    @Override
    public boolean checkSession(String key) {
        SessionEntity sessionEntity = cache.get(key);
        if (Optional.ofNullable(sessionEntity).isPresent()) {
            this.saveSession(key, sessionEntity);
            return true;
        }
        return false;
    }

    @Override
    public void removeSession(String key) {
        cache.remove(key);
    }
}
