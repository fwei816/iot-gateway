package org.thanos.iot.service.login;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.thanos.iot.controller.login.LoginVo;
import org.thanos.iot.controller.menu.MenuVo;
import org.thanos.iot.entity.session.SessionEntity;
import org.thanos.iot.entity.user.IotUser;
import org.thanos.iot.entity.user.IotUserRole;
import org.thanos.iot.mapper.role.IotRoleMenuMapper;
import org.thanos.iot.mapper.user.IotUserMapper;
import org.thanos.iot.mapper.user.IotUserRoleMapper;
import org.thanos.iot.service.menu.MenuService;
import org.thanos.iot.service.session.SessionService;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service("loginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    private IotUserMapper userMapper;

    @Autowired
    private IotUserRoleMapper userRoleMapper;

    @Autowired
    private IotRoleMenuMapper roleMenuMapper;

    @Autowired
    private MenuService menuService;

    @Autowired
    private SessionService sessionService;

    @Override
    public LoginVo login(String loginId, String password) {
        LoginVo loginVo = new LoginVo();
        IotUser user = userMapper.selectByLoginId(loginId, null);
        Assert.isTrue(Optional.ofNullable(user).isPresent(), "user is not exists");
        Assert.isTrue(user.getPassword().equals(password), "password is not equals");
        IotUserRole commonUserRole = userRoleMapper.selectByUserId(user.getId());
        if (Objects.nonNull(commonUserRole)) {
            loginVo.setRoleId(commonUserRole.getRoleId());
            List<Integer> menuIds = roleMenuMapper.queryByRoleId(commonUserRole.getRoleId());
            if (CollectionUtils.isNotEmpty(menuIds)) {
                List<MenuVo> menuVos = menuService.queryByMenuIds(menuIds);
                loginVo.setMenus(menuVos);
            }
        }
        String token = UUID.randomUUID().toString();
        SessionEntity entity = new SessionEntity();
        entity.setId(user.getId());
        entity.setLoginId(user.getLoginId());
        entity.setUuid(token);
        long period = sessionService.saveSession(token, entity);
        loginVo.setToken(token);
        loginVo.setTokenPeriod(period);
        return loginVo;
    }

    @Override
    public boolean checkSession(String token) {
        return sessionService.checkSession(token);
    }

    @Override
    public void logout(String token) {
        sessionService.removeSession(token);
    }
}
