package org.thanos.iot.service.role;

import org.springframework.validation.annotation.Validated;
import org.thanos.iot.controller.role.RoleVo;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Validated
public interface RoleService {

    void addRole(@Valid @NotBlank String code, List<Integer> menuIds);

    void modifyRole(@Valid @NotNull Integer id, @Valid @NotBlank String code, List<Integer> menuIds);

    void deleteRole(@Valid @NotNull Integer roleId);

    RoleVo discoverOne(@Valid @NotNull Integer roleId);

    List<RoleVo> discoverRoles(String code, Long startTime, Long endTime);
}
