package org.thanos.iot.service.login;

import org.thanos.iot.controller.login.LoginVo;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@Validated
public interface LoginService {

    LoginVo login(@Valid @NotBlank String loginId, @Valid @NotBlank String password);

    boolean checkSession(@Valid @NotBlank String token);

    void logout(@Valid @NotBlank String token);
}
