package org.thanos.iot.controller.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.thanos.iot.service.login.LoginService;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1.0")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping(value = "/login")
    public LoginVo login(@RequestBody LoginVo loginVo, HttpServletResponse response) {
        try {
            LoginVo result = loginService.login(loginVo.getLoginId(), loginVo.getPassword());
            response.setStatus(HttpStatus.OK.value());
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @GetMapping(value = "/logout")
    public void logout(@RequestHeader(HttpHeaders.AUTHORIZATION) String token, HttpServletResponse response) {
        try {
            loginService.logout(token);
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
