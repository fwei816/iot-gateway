package org.thanos.iot.controller.login;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.thanos.iot.controller.menu.MenuVo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginVo {

    @JsonProperty("loginId")
    private String loginId;

    @JsonProperty("password")
    private String password;

    @JsonProperty("loginFrom")
    private Integer loginFrom;

    @JsonProperty("token")
    private String token;

    @JsonProperty("tokenPeriod")
    private long tokenPeriod;

    @JsonProperty("appLimit")
    private Integer appLimit;

    @JsonProperty("roleId")
    private Integer roleId;

    @JsonProperty("constructionId")
    private Integer constructionId;

    @JsonProperty("menus")
    private List<MenuVo> menus;
}
