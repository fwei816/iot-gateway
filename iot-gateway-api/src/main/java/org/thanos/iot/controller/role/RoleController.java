package org.thanos.iot.controller.role;

import org.thanos.iot.service.role.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1.0")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @PutMapping(path = "/role")
    public void addRole(@RequestBody RoleVo roleVo, HttpServletResponse response) {
        try {
            roleService.addRole(roleVo.getCode(), roleVo.getMenuIds());
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }


    @PostMapping(path = "/role")
    public void updateRole(@RequestBody RoleVo roleVo, HttpServletResponse response) {
        try {
            roleService.modifyRole(roleVo.getId(), roleVo.getCode(), roleVo.getMenuIds());
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    @GetMapping(path = "/roles")
    public List<RoleVo> roles(@RequestParam(value = "code", required = false) String code,
                              @RequestParam(value = "startTime", required = false) Long startTime,
                              @RequestParam(value = "endTime", required = false) Long endTime,
                              HttpServletResponse response) {
        try {
            List<RoleVo> roles = roleService.discoverRoles(code,startTime,endTime);
            response.setStatus(HttpStatus.OK.value());
            return roles;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @GetMapping(path = "/role/{id}")
    public RoleVo role(@PathVariable("id") Integer id, HttpServletResponse response) {
        try {
            RoleVo role = roleService.discoverOne(id);
            response.setStatus(HttpStatus.OK.value());
            return role;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @DeleteMapping(path = "/role/{id}")
    public void deleteRole(@PathVariable("id") Integer id, HttpServletResponse response) {
        try {
            roleService.deleteRole(id);
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
