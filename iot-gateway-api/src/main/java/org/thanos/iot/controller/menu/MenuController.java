package org.thanos.iot.controller.menu;

import org.thanos.iot.service.menu.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1.0")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @GetMapping(path = "/menus")
    public List<MenuVo> menus(HttpServletResponse response) {
        try {
            List<MenuVo> menuVos = menuService.queryAll();
            response.setStatus(HttpStatus.OK.value());
            return menuVos;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @GetMapping(path = "/menuList")
    public List<MenuVo> menuList(HttpServletResponse response) {
        try {
            List<MenuVo> result = menuService.limitMenuList();
            response.setStatus(HttpStatus.OK.value());
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }
}
