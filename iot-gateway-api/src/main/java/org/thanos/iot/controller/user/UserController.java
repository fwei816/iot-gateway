package org.thanos.iot.controller.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.thanos.iot.service.user.UserService;
import org.thanos.iot.util.page.GridModel;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1.0")
public class UserController {

    @Autowired
    private UserService userService;

    @PutMapping(path = "/user")
    public void addUser(@RequestBody UserVo userVo, HttpServletResponse response) {
        try {
            userService.addUser(userVo.getLoginId(), userVo.getPassword(), userVo.getName(), userVo.getTelephone(), userVo.getEmail(), userVo.getRoleId());
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    @PostMapping(path = "/user")
    public void modifyUser(@RequestBody UserVo userVo, HttpServletResponse response) {
        try {
            userService.modifyUser(userVo.getId(), userVo.getName(), userVo.getTelephone(), userVo.getEmail(), userVo.getRoleId());
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    @PostMapping(path = "/user-password")
    public void modifyPassword(@RequestBody UserVo userVo, HttpServletResponse response) {
        try {
            userService.modifyUserPassword(userVo.getId(), userVo.getPassword());
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    @GetMapping(path = "/user/{id}")
    public UserVo discoverUser(@PathVariable("id") Integer id, HttpServletResponse response) {
        try {
            UserVo userVo = userService.discoverUser(id);
            response.setStatus(HttpStatus.OK.value());
            return userVo;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @GetMapping(path = "/user")
    public boolean discoverUserByLoginId(@RequestParam("loginId") String loginId, @RequestParam(value = "id", required = false) Integer id, HttpServletResponse response) {
        try {
            boolean result = userService.discoverUserByLoginId(loginId, id);
            response.setStatus(HttpStatus.OK.value());
            return result;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return false;
        }
    }

    @GetMapping(path = "/users")
    public GridModel<UserVo> discoverUsers(@RequestParam(value = "name", required = false) String name, @RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize, HttpServletResponse response) {
        try {
            GridModel<UserVo> userVo = userService.discoverUsers(name, pageNum, pageSize);
            response.setStatus(HttpStatus.OK.value());
            return userVo;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            return null;
        }
    }

    @DeleteMapping(path = "/user/{id}")
    public void deleteUser(@PathVariable("id") Integer id, HttpServletResponse response) {
        try {
            userService.deleteUser(id);
            response.setStatus(HttpStatus.OK.value());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
}
