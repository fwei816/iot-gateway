package org.thanos.iot.controller.monitor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1.0")
public class MonitorController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @GetMapping(path = "/message")
    public void sendMessage() {
        messagingTemplate.convertAndSend("/topic/public", UUID.randomUUID().toString());
        log.info("send message success");
    }
}
