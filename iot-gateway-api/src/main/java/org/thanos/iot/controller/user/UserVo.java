package org.thanos.iot.controller.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.thanos.iot.controller.role.RoleVo;

@Data
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserVo {

    private Integer id;
    private String loginId;
    private String password;
    private String name;
    private String telephone;
    private String email;
    private Integer roleId;
    private RoleVo roleVo;


}
