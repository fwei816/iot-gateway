package org.thanos.iot.controller.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;


@Data
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuVo {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("path")
    private String path;
    @JsonProperty("operation")
    private String operation;
    @JsonProperty("parentId")
    private Integer parentId;
    @JsonProperty("permits")
    private List<String> permits;
    @JsonProperty("children")
    private List<MenuVo> children;

}