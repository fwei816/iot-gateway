# common-platform
## 介绍
* 通用公共平台，因为大部分系统都有用户，权限，这里使用的是通用权限模型。
* 为有些需要用户的项目做基础。
## 技术
* springboot
* mybatis
* intetceptor, ehcache作session缓存
* poi 导入导出 excel
* RDBC权限模型
* React, ant design, echarts, axios
### common-platform-api
* 后端服务
### common-platform-web
* React 的前端项目
